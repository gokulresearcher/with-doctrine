<?php

namespace App\Controller;

use App\Entity\Invitation;
use App\Repository\InvitationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InvitationController extends AbstractController
{
    private $repository;

    public function __construct(InvitationRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/invitation/getall", name="invitation-getall")
     */
    public function index(): Response
    {
        $result = $this->repository->findAll();

        $jsonData = [];
        foreach ($result as $invitation) {
            $data['id'] = $invitation->getId();
            $data['name'] = $invitation->getSenderName();
            $data['adddress'] = $invitation->getAddress();

            $jsonData[] = $data;
        }

        return $this->json($jsonData);
    }

    /**
     * @Route("/invitation/create", name="invitation-create")
     */
    public function createInvitation(Request $request): Response
    {
        $invitationData = json_decode($request->getContent(), true);

        $invitation = new Invitation();
        $invitation->setSenderName($invitationData['name'] ?? 'no-name');
        $invitation->setAddress($invitationData['address'] ?? 'no-address');

        $id = $this->repository->createInvitation($invitation);

        return $this->json(['success' => $id], Response::HTTP_CREATED);
    }
}
